//
//  DetailViewController.h
//  01-Example
//
//  Created by Michael Ziray on 8/26/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

