//
//  ViewController.swift
//  03-MapView
//
//  Created by Michael Ziray on 9/2/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.mapView.mapType = MKMapType.Standard
        
        LocationController.startReportingLocation()
        
        let mapAnnotation = MKPointAnnotation()
        mapAnnotation.title = "My location"
        mapAnnotation.coordinate = CLLocationCoordinate2DMake(40.72852712420599, -74.89105224609375)
        
        mapView.addAnnotation( mapAnnotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

