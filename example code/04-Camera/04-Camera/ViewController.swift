//
//  ViewController.swift
//  04-Camera
//
//  Created by Michael Ziray on 9/14/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var libraryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func showLibraryAction(sender: AnyObject)
    {
        let imagePickerController = UIImagePickerController()
        
        libraryButton.titleLabel?.text = "Clicked"
        
        imagePickerController.delegate = self
        
        if( UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
        }
        else
        {
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        imagePickerController.allowsEditing = true

        self.presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        imageView.image = image
        
        self.dismissViewControllerAnimated( true, completion:  nil)
    }
    
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
//    {
//        let imageDate = info[UIImagePickerControllerEditedImage] as! UIImage
//        
//        imageView.image = imageDate
//        
//        self.dismissViewControllerAnimated( true, completion:  nil)
//    }
    
    
    
    
    
    
    
    
    
    
    
    
}

