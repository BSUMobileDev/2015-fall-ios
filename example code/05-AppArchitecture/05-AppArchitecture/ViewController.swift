//
//  ViewController.swift
//  05-AppArchitecture
//
//  Created by Michael Ziray on 9/16/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showView(sender: AnyObject) {
        if( sender.tag == 0 ){
            println("Home!")
            
            let aboutVC = self.storyboard?.instantiateViewControllerWithIdentifier("AboutViewController") as! AboutViewController
            self.presentViewController(aboutVC, animated:  true, completion: nil)
            
            aboutVC.whichButton = sender.tag
        }
        else if sender.tag == 1{
            println("Work")
        }
        else{
            println("Else")
        }
    }

}

