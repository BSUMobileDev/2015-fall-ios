//
//  Building+CoreDataProperties.swift
//  06-CoreDataTableViews
//
//  Created by Michael Ziray on 9/30/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Building {

    @NSManaged var buildingName: String?
    @NSManaged var buildingLocation: NSManagedObject?

}
