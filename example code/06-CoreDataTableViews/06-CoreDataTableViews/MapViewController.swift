//
//  MapViewController.swift
//  06-CoreDataTableViews
//
//  Created by Michael Ziray on 10/11/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit

class MapViewController:UIViewController
{
    @IBAction func addTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("DID_ADD", object: nil)
    }
    
}
