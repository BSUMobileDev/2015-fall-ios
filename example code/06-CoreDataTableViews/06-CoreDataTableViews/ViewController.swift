//
//  ViewController.swift
//  06-CoreDataTableViews
//
//  Created by Michael Ziray on 9/30/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let universityArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let building = NSEntityDescription.insertNewObjectForEntityForName( "Building", inManagedObjectContext: appDelegate.managedObjectContext) as! Building
        
        building.buildingName = "MEC"
        
        
        let buildingLocation = NSEntityDescription.insertNewObjectForEntityForName("BuildingLocation", inManagedObjectContext: appDelegate.managedObjectContext) as! BuildingLocation
        
        buildingLocation.latitude = 123.8877
        buildingLocation.longitude = 123.123
        
        building.buildingLocation = buildingLocation
        
        
        universityArray.addObject( building )
        
        appDelegate.saveContext()
    }
    
    
    @IBAction func addTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("DID_ADD", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( section == 0 ){
            return universityArray.count
        }
        else{
            return universityArray.count
        }
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if( section == 0 ){
            return "Normal"
        }
        else {
            return "Section 2 header"
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

        let cell = UITableViewCell()
        
        if( indexPath.section == 0 ){
            let universityObject = universityArray[indexPath.row] as! Building
            cell.textLabel?.text = universityObject.buildingName
            cell.detailTextLabel?.text = "latitude here"
        }
        else{
            let universityName = universityArray[indexPath.row]
            cell.textLabel?.text = " section 2"
        }
        
        
        
        cell.accessoryType = .DisclosureIndicator
        return cell
    }

}












