﻿using UnityEngine;
using System.Collections;

public class PlayScaryNoise : MonoBehaviour {

	private AudioSource mockingSource;

	// Use this for initialization
	void Awake () {
		mockingSource = GetComponent<AudioSource> ();
		Debug.Log ("On Awake called");
	}

	void Start()
	{
		Debug.Log ("Start is called");
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider otherObject)
	{
		Debug.Log ("Object entered the trigger");
		float volume = Random.Range (0.3f, 0.75f);
		mockingSource.Play();
	}

	void OnTriggerStay(Collider otherObject)
	{
		Debug.Log ("Object is within the trigger");
	}

	void OnTriggerExit(Collider otherObject)
	{
		Debug.Log ("Object exited the trigger");
		mockingSource.Stop ();
	}
}
