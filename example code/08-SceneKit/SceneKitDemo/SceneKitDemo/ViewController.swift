//
//  ViewController.swift
//  SceneKitDemo
//
//  Created by Michael Ziray on 10/28/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit
import SceneKit


class ViewController: UIViewController {

    @IBOutlet weak var sceneView: SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        sceneView.scene = SCNScene(named: "tie-fighter.dae")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

