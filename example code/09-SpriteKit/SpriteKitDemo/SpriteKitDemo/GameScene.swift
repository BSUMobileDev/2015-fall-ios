//
//  GameScene.swift
//  SpriteKitDemo
//
//  Created by Michael Ziray on 10/28/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var spaceShip:SKSpriteNode = SKSpriteNode()
    var jetFlames:SKEmitterNode = SKEmitterNode()
    
    override func didMoveToView(view: SKView) {
        spaceShip = childNodeWithName("Spaceship") as! SKSpriteNode
        spaceShip.setScale(0.5)
        
        jetFlames = NSKeyedUnarchiver.unarchiveObjectWithFile(NSBundle.mainBundle().pathForResource("fire", ofType: "sks")!) as! SKEmitterNode
        
        jetFlames.position = CGPointMake(0,0-spaceShip.frame.size.height);
        jetFlames.particleBirthRate = 0;
        spaceShip.addChild(jetFlames)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        jetFlames.particleBirthRate = 500
        
      spaceShip.physicsBody?.velocity = CGVectorMake(0.0, 500.0)
    }

    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        jetFlames.particleBirthRate = 0
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
