//
//  ViewController.swift
//  10-UIKitchenSink
//
//  Created by Michael Ziray on 11/11/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var searchTextfield: UITextField!
    
    @IBOutlet weak var progressSlider: UISlider!
    
    @IBOutlet weak var loadingBar: UIProgressView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        let localNotification:UILocalNotification = UILocalNotification()
        
        localNotification.alertBody = "Time for class!"
        localNotification.fireDate  = NSDate(timeIntervalSinceNow: 5)
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "longPress:")
        
        longPressGesture.minimumPressDuration = 1.2
        
        self.view.addGestureRecognizer(longPressGesture)
        
        
        searchSegmentedControl.addTarget(self, action: "segmentedControlChanged", forControlEvents: UIControlEvents.ValueChanged)
        
        progressSlider.addTarget(self, action: "sliderDidChange", forControlEvents: .ValueChanged)
        
        self.activityIndicator.alpha = 0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "textfieldDidChange", name: UITextFieldTextDidChangeNotification, object: self.searchTextfield)
    }
    
    func textfieldDidChange()
    {
        print( self.searchTextfield.text )
    }
    
    
    func sliderDidChange()
    {
        self.loadingBar.progress = self.progressSlider.value
    }
    
    
    
    func segmentedControlChanged()
    {
        print( self.searchSegmentedControl.selectedSegmentIndex )
        
        switch( self.searchSegmentedControl.selectedSegmentIndex )
        {
        case 0:
            self.searchTextfield.placeholder = "Buildings Search"
            break;
        case 1:
            self.searchTextfield.placeholder = "Location Search"
            break;
        case 2:
            self.searchTextfield.placeholder = "Department Search"
            break;
        case 3:
            self.searchTextfield.placeholder = "All Search"
            break;
        default:
            self.searchTextfield.placeholder = "All Search"
        }
    }
    
    
    
    func longPress( gestureRecognizer:UILongPressGestureRecognizer )
    {
        if (gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed)
        {
            let location:CGPoint = gestureRecognizer.locationInView( gestureRecognizer.view)
            let menuController:UIMenuController = UIMenuController.sharedMenuController()
            let resetMenuItem:UIMenuItem = UIMenuItem(title: "Remove this item", action: "removeThisItem")
            
            menuController.menuItems = [resetMenuItem]
            menuController.setTargetRect(CGRectMake(location.x, location.y, 0.0, 0.0), inView: gestureRecognizer.view!)
            menuController.setMenuVisible(true, animated: true)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }


}

