//
//  AppDelegate.swift
//  Project1
//
//  Created by Michael Ziray on 9/21/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
    
        
        LocationController.startReportingLocation()
        

        Alamofire.request(.GET, "http://zstudiolabs.com/labs/compsci402/buildings.json").response { request, response, data, error in
            
//            print(request)
//            print(response)
//            print(error)
            
            do {
                let buildingsArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                print(buildingsArray)
                
                for( var index = 0; index < buildingsArray.count; index++ )
                {
                    let newPin:LocationPin = LocationPin()
                    
                    newPin.pinTitle = buildingsArray[index]["name"] as! String
                    newPin.pinDescription = buildingsArray[index]["description"] as! String
                    let locationCoords  = buildingsArray[index]["location"] as! NSDictionary
                    
                    var newLocation: CLLocationCoordinate2D = CLLocationCoordinate2D ()
                    newLocation.latitude  = locationCoords["latitude"] as! Double
                    newLocation.longitude = locationCoords["longitude"] as! Double
                    
                    newPin.pinLocation = newLocation
                    
                    LocationsController.addPin( newPin )
                }
                // use anyObj here
            } catch {
                print("json error: \(error)")
            }


        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

