//
//  DetailsViewController.swift
//  Project1
//
//  Created by Michael Ziray on 9/21/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit
import MapKit

class DetailsViewController : UIViewController
{
    @IBOutlet weak var titleTextfield: UITextField!
    
    @IBOutlet weak var descriptionTextfield: UITextField!
    

    
    @IBAction func savePin(sender: AnyObject) {
        
        let title = titleTextfield!.text
        let descriptionString = descriptionTextfield!.text
        
        
        let currentLocationCoordinate:CLLocationCoordinate2D  = CLLocationCoordinate2D.init(latitude: LocationController.currentLocation.coordinate.latitude, longitude: LocationController.currentLocation.coordinate.longitude)
        
        let currentLocation = LocationPin()
        currentLocation.pinTitle = title!
        currentLocation.pinDescription = descriptionString!
        currentLocation.pinLocation = currentLocationCoordinate

        LocationsController.addPin( currentLocation )

        self.navigationController?.popViewControllerAnimated(true)
    }
    
}



