//
//  LocationController.swift
//  03-MapView
//
//  Created by Michael Ziray on 9/9/15.
//  Copyright (c) 2015 Z Studio Labs. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class LocationController: NSObject, CLLocationManagerDelegate
{
    static var locationController = LocationController()
    
    let locationManager:CLLocationManager = CLLocationManager()
    
    static var currentLocation:CLLocation = CLLocation()

    class func startReportingLocation()
    {
        locationController.locationManager.startUpdatingLocation()
    }
    
    class func stopReportingLocation()
    {
        
    }
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.distanceFilter = 0.0
        locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print(status)
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {

        LocationController.currentLocation = newLocation
    }
}