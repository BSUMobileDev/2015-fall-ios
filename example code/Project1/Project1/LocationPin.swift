//
//  Location.swift
//  Project1
//
//  Created by Michael Ziray on 9/21/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import Foundation
import MapKit

class LocationPin : NSObject
{
    var pinTitle:String = "Not initialized"
    var pinDescription:String = ""
    var pinLocation:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: 0, longitude: 0)
    
}