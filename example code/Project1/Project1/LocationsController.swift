//
//  LocationsController.swift
//  Project1
//
//  Created by Michael Ziray on 9/21/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import Foundation

class LocationsController
{
    static var locationsArray = NSMutableArray()
    
    class func addPin( newPin: LocationPin)
    {
        locationsArray.addObject( newPin )
        
        NSNotificationCenter.defaultCenter().postNotificationName("NewPin", object: newPin)
    }
}