//
//  LocationsTableViewController.swift
//  Project1
//
//  Created by Michael Ziray on 9/30/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit

class LocationsTableViewController : UIViewController, UITableViewDataSource//, UITableViewDelegate
{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellDq = tableView.dequeueReusableCellWithIdentifier("cell")
        cellDq!.textLabel?.text = "Hello"
        
        return cellDq!
    }
}
