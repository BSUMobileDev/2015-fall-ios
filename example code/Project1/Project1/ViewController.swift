//
//  ViewController.swift
//  Project1
//
//  Created by Michael Ziray on 9/21/15.
//  Copyright © 2015 Z Studio Labs. All rights reserved.
//

import UIKit

import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "newPinAdded:", name: "NewPin", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override
    func viewDidAppear(animated: Bool) {
//        for( var index = 0; index < LocationsController.locationsArray.count; index++)
//        {
//            let currentLocation = LocationsController.locationsArray[index] as! LocationPin
//            
//            let pin: MKPointAnnotation = MKPointAnnotation()
//            pin.title       = currentLocation.pinTitle
//            pin.subtitle    = currentLocation.pinDescription
//            pin.coordinate  = currentLocation.pinLocation
//            
//            mapView.addAnnotation( pin )
//            
//        }
    }
    
    func newPinAdded( notification:NSNotification )
    {
        let newPin : LocationPin = notification.object as! LocationPin
        
        let pin: MKPointAnnotation = MKPointAnnotation()
        pin.title = newPin.pinTitle
        pin.coordinate = CLLocationCoordinate2D(latitude: newPin.pinLocation.latitude, longitude: newPin.pinLocation.longitude)
        mapView.addAnnotation( pin )
    }
    
//    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        let detailsViewController = segue.destinationViewController as! DetailsViewController
//        
//        detailsViewController.parentViewController
//    }
    
    
    
    
}

